
#include "detect_plate.h"

list<Car*> cars;
int ID = 1;           	

String cascade_name = "cascade.xml"; CascadeClassifier cascade;  																	// zvolena natrenovana kaskada

Rect enlargeROI(Mat frm, Rect boundingBox, int padding) {																			// zvetseni detekovaneho bounding boxu
    Rect returnRect = Rect(boundingBox.x - padding, boundingBox.y , boundingBox.width + (padding * 2), boundingBox.height );
    if (returnRect.x < 0)returnRect.x = 0;
    if (returnRect.y < 0)returnRect.y = 0;
    if (returnRect.x+returnRect.width >= frm.cols)returnRect.width = frm.cols-returnRect.x;
    if (returnRect.y+returnRect.height >= frm.rows)returnRect.height = frm.rows-returnRect.y;
    return returnRect;
}

detect_plate detect_spz(Mat drawing, Mat frame_original, VideoCapture capture, Ptr<ml::KNearest> knn[], ofstream& plates, vector<string>& list_spz, bool isReidentifikace){

	vector<vector<Point> > contours;
    vector<Rect> matches;
    vector<Mat> rois;

    int h = -1;
    h = capture.get(CV_CAP_PROP_POS_FRAMES);					
    detect_plate result;
    result.original = frame_original;
    
    if(!cascade.load(cascade_name)){							 							// nacteni kaskady pro detekci spz
		cout << "Nelze nacist klasifikator!" << endl;
		exit(1);  
	}; 
    for (auto c = cars.begin(); c != cars.end();) {
		if (h - (*c)->frame > 25) {															// půlsekundy					
			list<string> known_matches;
			for(auto p = list_id_spz.begin(); p != list_id_spz.end();){
				if((*p).first == (*c)->id){
					known_matches.push_back((*p).second);
					p = list_id_spz.erase(p);
				}	
				else{
					p++;
				}
		}
			
		string spztka = (final_plate(known_matches));										// vybrani finalni spz na principu nejcastejsiho znaku ve sloupci SPZ
	
		if(spztka.length() == 7){
			if(isReidentifikace == true){													// pracujeme v modu reidentifikace, budou vypisovany pouze reidentifikovane SPZ
				string respztka = reidentification(spztka, list_spz);	
				if(respztka.length() == 7){
					plates << respztka << endl;	
					cout << "SPZ-> "<< respztka << " <-- REIDENTIFIKOVANA" << endl;			// FINALNI STATNI POZNAVACI ZNACKA
				}
				else{
					cout << "SPZ-> "<< spztka << endl;
				}
			}
			else{																			// nachazime se v modu detekce, budou vypisovany vsechny detekovane SPZ
				plates << spztka << endl;	
				cout << "SPZ-> "<< spztka << endl;											// finalni SPZ
			}		
		}
		        
        c = cars.erase (c);																	// posledni aktualizace polohy je starsi nez pul sekundy
      }
      else {
        c++;																				// posune pointer, ukazuje na nasledujici auto				projde vsechny co projizdi aby ty stare mohl smazat
       
      }
    }

    findContours(drawing, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));					// kontury boxu vozidel

    for (int i = 0; i< contours.size(); i++){
        if(contourArea(contours[i]) > (20000 / (SCALE * SCALE))){										
            Rect bounding_rect = boundingRect(contours[i]);
			
			int new_height = std::max(MIN_HEIGHT, bounding_rect.height / 3);		
			if(bounding_rect.height > MIN_HEIGHT){
				bounding_rect.y += bounding_rect.height - new_height;
				bounding_rect.height = new_height;
			}
			int new_width = std::max(MIN_WIDTH, (bounding_rect.width / (2)));	
			
			if(bounding_rect.width > MIN_WIDTH){
				bounding_rect.x += (bounding_rect.width - new_width) / 2;
				bounding_rect.width = new_width;
			}
            bounding_rect.x *= SCALE; bounding_rect.y *= SCALE;
            bounding_rect.width *= SCALE; bounding_rect.height *= SCALE;
            
            if(bounding_rect.y >= 540){
				rois.push_back( result.original(bounding_rect));                  		// vlozeni hodnot boxu do vectoru	
			}           
        }
    }
    
	Mat roi;
    for (auto const& roi : rois) {    
        Size whole_size;
        Point offset;
        vector<Rect> current_matches;
        roi.locateROI(whole_size, offset);
        current_matches.clear();
        cascade.detectMultiScale(roi, current_matches, 1.2, 5,  CV_HAAR_FIND_BIGGEST_OBJECT , Size(40, 40), Size(140,140));    // mozna 1.3

       for (auto& match : current_matches) {													// prepocitani pozice do puvodniho snimku
             match.x += offset.x;
             match.y += offset.y;
       }
           matches.insert(end(matches), begin(current_matches), end(current_matches));
    }
    
	char str[20];
    Mat crop_tmp, clone;
    
    for (Rect r:matches){								// prochazi boxy
      bool match = false;								// boxu zatim nebylo prirazeno zadne auto
      for (Car *c:cars) {
          if (norm(r.tl()-c->spz.tl()) < 100) {			// absolutni hodnota vzdalenosti  mezi posledni SPZ a nove prichozi mensi nez 100px
            c->spz = r; 
            c->frame = h;
            match = true;
  
		    Rect large = enlargeROI( result.original, c->spz, 15);                       		 // zveseni vyrezu, ktery zobrazuje celou spz bez orezu
            result.crop = result.original(large);	
            
			extracted_plate result2;
			result2 = ext(result.crop);															// segmentace znacky -> oriznuti a ocisteni od okolnich cernych car
			    
		    if(((3*(result2.image.rows)) > (result2.image.cols)) || ((result2.image.rows) > (result2.image.cols))){
				//cout << "Vyska je vetsi nez sirka nebo sirka je mensi nez 3x vyska" << endl;
			}
			else{
					int ID = c->id;
					extract_symbols(result2.image,result2.mask,knn, ID);					// extrahovani symbolů

			}
			rectangle( frame_original, r, Scalar(255,0,0), 5);	
            sprintf(str,"ID:%d",c->id);
            putText( result.original, str, c->spz.tl(), FONT_HERSHEY_PLAIN, 5,  Scalar(0,0,255,255), 5, 50);           

		}
			
    }
	if (!match) {						// pokud prijede prvni auto nebo nove, match je false, skoci to sem, nastavi to do pozici, frame, zvysi ID a vypise
      Car *c = new Car();
      c->spz = r;
      c->frame = h;
      c->id = ID++;
      cars.push_back (c);				// ulozi hodnoty do seznamu cars
      char str[20];
      sprintf(str,"ID:%d",c->id);
      putText( result.original, str, c->spz.tl(), FONT_HERSHEY_PLAIN, 2,  Scalar(0,0,255,255), 3);		// c->spz.tl()
	}
  }

    return result;

}
