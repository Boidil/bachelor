#ifndef DETECT_PLATE_H
#define DETECT_PLATE_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <iostream>
#include <list>

#include "main.h"
#include "segment.h"
#include "symbol.h"
#include "choose_plate.h"
#include "reidentification.h"

// ====================================================================

using namespace std;
using namespace cv;
     
// ====================================================================         						         		     
         	
struct detect_plate
{
    cv::Mat original;
    cv::Mat crop;
};       	
         	
// ====================================================================         						         		
         		
typedef struct car{
  Rect spz;
  Rect bounding_rect;
  int frame;
  int id;
} Car;         		
         						
// ====================================================================         						
         		
int const MIN_HEIGHT(30);         											 
int const MIN_WIDTH(50);         		
         						
// ====================================================================         						         		   
         						
detect_plate detect_spz( Mat, Mat, VideoCapture, Ptr<ml::KNearest> knn[], ofstream& plates,  vector<string>& list_spz, bool isReidentifikace );

#endif
