#include "segment.h"

double clip_limit_(2.0);
Size tile_grid_size_(4, 4);
Ptr<CLAHE> normalizer_ = cv::createCLAHE(clip_limit_,tile_grid_size_);	// adaptivni histogram equalization


// ===================== H I S T O G R A M S ===========================

// ---------------------------------------------------------------------
Mat get_histogram(Mat const& image, Mat const& mask, int32_t width, float height)
{
    int32_t histogram_size[] = { width };
    float histogram_range_0[] = { 0, static_cast<float>(height) };
    float const* histogram_ranges[] = { histogram_range_0 };
    int32_t histogram_channels[] = { 0 };

    Mat histogram;
    calcHist(&image, 1 			
        , histogram_channels
        , mask
        , histogram
        , 1 					// Dimensions
        , histogram_size
        , histogram_ranges
        , true 					
        , false); 				

    return histogram;
}

// =====================================================================
// =====================================================================

void filter_mask(cv::Mat const& mask, cv::Mat const& kernel, std::vector<int> ops)
{
    for (int op : ops) {
        cv::morphologyEx(mask, mask, op, kernel);						// pouziti parametru op, kolikrat je potreba
    }
}
// ----------------------------------------------------------------------------
cv::Point2f intersection(cv::Point2f const& A, double alpha
    , cv::Point2f const& B, double beta)
{
    alpha *= CV_PI / 180;
    beta *= CV_PI / 180;

    float f_AC(static_cast<float>(std::sin(alpha)));
    float g_AC(static_cast<float>(std::cos(alpha)));
    float f_BC(static_cast<float>(std::sin(beta)));
    float g_BC(static_cast<float>(std::cos(beta)));

    float det((f_BC * g_AC) - (f_AC * g_BC));

    float dx(B.x - A.x);
    float dy(B.y - A.y);

    float s_AC((f_BC * dy - g_BC * dx) / det);
    float s_BC((f_AC * dy - g_AC * dx) / det);

    cv::Point2f C;
    C.x = B.x + f_BC * s_BC;
    C.y = B.y + g_BC * s_BC;

    return C;
}
// ----------------------------------------------------------------------------
double mode(cv::Mat const& image, cv::Mat const& mask)
{
    int32_t const WIDTH(256);
    float const HEIGHT((image.type() == CV_32FC1) ? 1.0f : 256.0f);
    cv::Mat histogram(get_histogram(image, mask, WIDTH, HEIGHT));

    float max_value(0);
    int32_t max_i(0);
    for (int32_t i(0); (i < WIDTH); ++i) {
        float value(histogram.at<float>(i));
        if (value > max_value) {
            max_value = value;
            max_i = i;
        }
    }
    if (image.type() == CV_32FC1) {
        return max_i / 255.0;
    }
    return max_i;
}
// ----------------------------------------------------------------------------
vector<Scalar> make_random_colors(int32_t n)					// nahodny vyber barvy
{
    vector<Scalar> colors(n);
    for (auto& color : colors) {
        color = Scalar(rand() & 255, rand() & 255, rand() & 255);
    }
    return colors;
}

// ============================================================================
// ============================================================================

line_info create_line_info(Point const& p1, cv::Point const& p2, int32_t label=0)
{
	
	line_info result;
	result.label = label;
    
    result.point[0] = p1;
    result.point[1] = p2;


    result.center = (p1 + p2) / 2.0;
	result.angle = std::atan2(p2.x - p1.x
        , p2.y - p1.y) * 180.0 / CV_PI;
         
    return result;       
                  
}

line_info create_line_info(Point2f center, double angle, int32_t length, int32_t label = 0)
{
    double theta(angle * CV_PI / 180);
    double a = cos(theta), b = sin(theta);

	line_info result;
	
	result.label = label;
	result.center = center;
	result.angle = angle;
	result.label = label;

    result.point[0] = cv::Point(cvRound(center.x + length * b)
        , cvRound(center.y + length * a));
    result.point[1] = cv::Point(cvRound(center.x - length * b)
        , cvRound(center.y - length * a));
        
    return result;    
}

plate_match create_plate_match(point_vector const& contour)
{
    plate_match result;

    result.contour_.push_back(contour);
    result.bounding_box_ = cv::minAreaRect(contour);
    result.convex_hull_points_.resize(1);

    cv::Point2f tmp_vertices[4];
    result.bounding_box_.points(tmp_vertices);
    result.bounding_box_points_.emplace_back(tmp_vertices, tmp_vertices + 4);
    cv::convexHull(contour, result.convex_hull_points_[0]);
    result.roi_ = cv::boundingRect(result.bounding_box_points_[0]);

    return result;
}

// ===========================================================================
// ===========================================================================
// ===========================================================================

Mat make_work_image(plate_match const& match, Size const& size, Rect const& roi, Mat const& input_image, Mat const& blend_mask, Mat const& plate_hull_mask) 	//
{
	
    Mat input_roi(input_image, match.roi_);
    
    input_roi.convertTo(input_roi, CV_32FC3, 1.0 / 255);			// stupni obrazek predeme do floating point a rozsahu 0.0-1.0  -> na 8 bitech neni dost rozdilnych urovni // komulace chyb a operace nebudou linearni

    Mat gray_image;
    cvtColor(input_roi, gray_image, COLOR_BGR2GRAY);
    Scalar const FILL_COLOR(mode(gray_image, plate_hull_mask));	 	//vytvorime dalsi obrazek, kterej je vyplnenej nejcastejsi hodnotou sedi ze vstupu (zamaskovaneho)  -> funkce mod

    Mat work_image(size, CV_32FC1, FILL_COLOR);	
    Mat work_image_view(work_image(roi));
    Mat blend_mask_view(blend_mask(roi));

    work_image_view = work_image_view.mul(1.0f - blend_mask_view);				// 1 - tvoje hodnota * hodnota pozadi, chceme jen to okolo : takze kazdy bod vynasobime (1.0-hodnota v masce) -> takze tam kde byla maska cerna to bude mit puvodni intenzitu
																								
    add(work_image_view, blend_mask_view.mul(gray_image), work_image_view);		// vezmu ten samy pixel z blend_mask a pixel puvodni  vynasobim ho a pak to sectu dohromady
																				//  takze alpha je koeficient, a je puvodni, b je pozadi -> vysledek = (1-alpha) * b + alpha * a  // kdyz jsou oba dva vstupy v rozmezi 0-1 a alpha v rozmezi 0-1 tak vysledek je taky v rozmezi 0-1
																				// alpha urcuje pomer kolik ze ma vzit z kazdeho vstupu
																				
    return work_image;
}

// ============================================================================

Mat make_blend_mask(plate_match const& match, Size const& size, Point const& draw_offset) 		// vytvorime masku na namichani vysledneho obrazku
{
    
    Mat blend_mask(size, CV_8UC1, Scalar(0));												
    fillPoly(blend_mask, match.bounding_box_points_, Scalar(255), CV_AA, 0, draw_offset);	

    blend_mask.convertTo(blend_mask, CV_32FC1, 1.0 / 255);

    Mat kernel( getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
    erode(blend_mask, blend_mask, kernel);
    GaussianBlur(blend_mask, blend_mask, Size(5, 5), 1.0);

    return blend_mask;															// obdelnik s mirne zakulacenymi rohy, ktery ma uporstred 1.0 a uplne na vne 0.0 … a na hranici plynuly prechod v sirce nekolika pixelu
}

// ============================================================================

double get_angle(plate_match const& match)
{
    float aspect_ratio(match.bounding_box_.size.width / match.bounding_box_.size.height);
    if (aspect_ratio > 1) {
        return match.bounding_box_.angle;
    }
    return match.bounding_box_.angle + 90.0;
}

// ============================================================================

extracted_plate extract_match(plate_match const& match, Mat const& input_image) 
{
    uint32_t const MARGIN_SIZE(4);
    extracted_plate result;
    
    if((match.roi_.x < 0) || (match.roi_.y < 0) || (match.roi_.br().x > input_image.cols)){
		return result;
	}
	
	if((match.roi_.height + match.roi_.y) >= input_image.rows){
		return result;
	}
     
    Rect work_image_roi(Point(MARGIN_SIZE, MARGIN_SIZE), match.roi_.size());	 
    Size work_image_size(work_image_roi.size());
    
    work_image_size.width += MARGIN_SIZE * 2; work_image_size.height += MARGIN_SIZE * 2;	// vypocitame velikost vysledneho obrazku a roi s mezirkou okolo

    Point draw_offset(-match.roi_.x, - match.roi_.y);										// offsety pro kresleni a vyrezavani
    Point draw_offset_pad(MARGIN_SIZE - match.roi_.x, MARGIN_SIZE - match.roi_.y);

    Mat plate_hull_mask(work_image_roi.size(), CV_8UC1, Scalar(0));											// prazdna maska
	fillPoly(plate_hull_mask, match.convex_hull_points_, Scalar(255), 8, 0, draw_offset);					// vykreslime convex hull,coz je ale ted to sami, protoze mame obldenik
    Mat blend_mask(make_blend_mask(match, work_image_size, draw_offset_pad));

    Mat work_image(make_work_image(match, work_image_size, work_image_roi, input_image, blend_mask, plate_hull_mask));

    Point2f rotation_center(match.bounding_box_.center);										// zname stred znacky, uhel, tak jen otocime ten obrazek a masku, tady je ten floating point uz hodne poznat
    
    rotation_center.x += draw_offset.x; rotation_center.y += draw_offset.y;						// posunout o offset bounding boxu		// origin byl v levem hornim rohu, ted pracujeme jen s boxem, takze to o tom musime posunout
   
    
	double rotation_angle(get_angle(match));													
    
    Mat transform_mat( getRotationMatrix2D(rotation_center, rotation_angle, 1));				// vytvoreni matice pro warp

    

    if (true) {										// rotate
        warpAffine(work_image, result.image, transform_mat, work_image_size, INTER_CUBIC, BORDER_REPLICATE);	// rotační metoda
        Mat rotated_mask;
        warpAffine(blend_mask, result.mask, transform_mat, work_image_size, INTER_CUBIC, BORDER_REPLICATE);		
    } else {
        result.image = work_image;
        result.mask = blend_mask;
    }

    return result;
}

// ============================================================================

void get_corners(line_info const& top_edge, line_info const& bottom_edge, line_info const& left_edge, line_info const& right_edge, 			// najde pruseciky 4 stran - rohy
				 Point2f& tl, Point2f& tr, Point2f& bl, Point2f& br)
{
    tl = intersection(top_edge.center, top_edge.angle
        , left_edge.center, left_edge.angle);
    tr = intersection(top_edge.center, top_edge.angle
        , right_edge.center, right_edge.angle);
    bl = intersection(bottom_edge.center, bottom_edge.angle
        , left_edge.center, left_edge.angle);
    br = intersection(bottom_edge.center, bottom_edge.angle
        , right_edge.center, right_edge.angle);
}

// ------------------------------------------------------------------------------

void refine_plate_match(plate_match& match, Mat const& edges, vector<line_info> const& lines_v, vector<line_info> const& lines_h)
{
	
	
    if ((lines_v.size() < 2) || (lines_h.size() < 2)){															// kontrola jestli mame v kazde orientaci aspon dve cary	    
		return;
	}

    Point2f tl, tr, bl, br;
    get_corners(lines_h.front(), lines_h.back(), lines_v.front(), lines_v.back(), tl, tr, bl, br);				// vezmeme prvni a posledni caru, prootze jsou setrideny zleva doprava a seshora dolu
																												// .front() a .back() na vektoru - reference na prvni a posledni prvek
    line_info top_line(create_line_info(tl, tr)); line_info bottom_line(create_line_info(bl, br));
    line_info left_line(create_line_info(tl, bl)); line_info right_line(create_line_info(tr, br));

    double horizontal_angle((top_line.angle + bottom_line.angle) / 2);											// je to trochu sejdrem, takze to srovname - horizontalnim dam  prumer jejich uhlu
    
    double vertical_angle(true ? (horizontal_angle - 90) : ((left_line.angle + right_line.angle) / 2));			// true false, mozna konfigurovatelny	// refine_to_rectangle_ ---- u vertikalnich tam byl vyber, bude taky vzit prumer, nebo je natvrdo dat do praveho uhlu vuci tem horizontalnim

    tl = intersection(top_line.center, horizontal_angle, left_line.center, vertical_angle);						// hnuly jsme s uhly, takze stavajici rohy neplati, takze je pocitame znovu
    tr = intersection(top_line.center, horizontal_angle, right_line.center, vertical_angle);
    bl = intersection(bottom_line.center, horizontal_angle, left_line.center, vertical_angle);
    br = intersection(bottom_line.center, horizontal_angle, right_line.center, vertical_angle);

    Mat bounding_poly(edges.clone());
    cvtColor(bounding_poly, bounding_poly, COLOR_GRAY2BGR);			// vykresleni
    
    Scalar color_final(0, 255, 255);
    line(bounding_poly, tl, tr, color_final, 3, CV_AA); line(bounding_poly, bl, br, color_final, 3, CV_AA);
    line(bounding_poly, tl, bl, color_final, 3, CV_AA); line(bounding_poly, tr, br, color_final, 3, CV_AA);

    Scalar color_orig(0, 0, 255);
    line(bounding_poly, top_line.point[0], top_line.point[1], color_orig, 1, CV_AA);
    line(bounding_poly, bottom_line.point[0], bottom_line.point[1], color_orig, 1, CV_AA);
    line(bounding_poly, left_line.point[0], left_line.point[1], color_orig, 1, CV_AA);
    line(bounding_poly, right_line.point[0], right_line.point[1], color_orig, 1, CV_AA);



    point_vector new_contour{tl, bl, br, tr};
	match = create_plate_match(new_contour);
	
}

// ============================================================================

inline double get_distance(cv::Point2f const& a, cv::Point2f const& b)
{
    float dx(a.x - b.x), dy(a.y - b.y);
    return std::sqrt((dx * dx) + (dy * dy));
}

// ============================================================================

void sort_edge_lines(vector<line_info>& lines, bool vertical)													// sestrideni podle x nebo y souradnic stredu	// TODO

{
    if (vertical) {
        sort(begin(lines), end(lines), [](line_info const& a, line_info const& b) -> bool{						// lambda bere dva parametry line_info const & a vraci bool
					return a.center.x < b.center.x;																// treti  parametr function object -> ktery implementuje kriterium pro trideni
			});
    } 
    else {
        sort(begin(lines), end(lines), [](line_info const& a, line_info const& b) -> bool{
                return a.center.y < b.center.y;
        });
    }																								// sort, das mu zacatek a konec sekvence kterou ma tridit a bud pouzijes std::less, ktery implicitne pouziva operator 
}																									

// ============================================================================

vector<line_info> reduce_edge_lines(vector<line_info> const& lines, int32_t label_count)			// pro kazdou caru vypocitam jednu caru, ktera na ni nejvic sedi -> vezme prumer stredu a prumer uhlu
{
    vector<int32_t> segment_counts(label_count, 0);
    vector<double> angles(label_count, 0.0);
    vector<Point2f> centers(label_count, Point(0, 0));

    for (uint32_t i(0); i < lines.size(); ++i) {								
                
        line_info const& l(lines[i]);
               
        ++segment_counts[l.label];
        
        angles[l.label] += l.angle;
        centers[l.label] += l.center;
    }
	
    vector<line_info> result;
	for (int32_t i(0); i < label_count; ++i) {
		
        angles[i] /= segment_counts[i];
        centers[i] /= segment_counts[i];

        result.emplace_back(create_line_info(centers[i], angles[i], 1000, i));
    }
    
    return result;
}

// ============================================================================
 
bool is_equal(const Vec4i& _l1, const Vec4i& _l2, bool vertical)								// vypocitame delky dvou car, jejich vzajemny uhel a bereme cary, ktere maji uhel v urcitem 
																								// limitu a cary ktere jsou od stredu vertik. vzdaleny v urcitem limitu  resp. horizontalne (pro svisle)
{
    Vec4i l1(_l1), l2(_l2);

    float dx1(static_cast<float>(l1[2] - l1[0])); float dy1(static_cast<float>(l1[3] - l1[1]));
    float dx2(static_cast<float>(l2[2] - l2[0])); float dy2(static_cast<float>(l2[3] - l2[1]));

    float length1 = std::sqrt(dx1*dx1 + dy1*dy1); float length2 = std::sqrt(dx2*dx2 + dy2*dy2);

    float product = dx1*dx2 + dy1*dy2;

    float cos_angle = std::fabs(product / (length1 * length2));
    float cos_angle_threshold(vertical? static_cast<float>(cos(CV_PI / 9)) : static_cast<float>(cos(CV_PI / 18)));
    
    if (cos_angle < cos_angle_threshold) {
        return false;
    }

    float mx1((l1[0] + l1[2]) * 0.5f); float mx2((l2[0] + l2[2]) * 0.5f);
    float my1((l1[1] + l1[3]) * 0.5f); float my2((l2[1] + l2[3]) * 0.5f);
    float dmx(mx1 - mx2), dmy(my1 - my2);

    return vertical ? (fabs(dmx) < 8.0) : (fabs(dmy) < 4.0);						// vraci skupiny car
}

// ============================================================================

vector<line_info> find_edge_lines(Mat const& edges, int32_t& label_count, bool vertical)
{
    using namespace placeholders;

    double line_threshold((vertical ? edges.rows : edges.cols) / 8);								//  line_threshold se vypocita automaticky, chce me cary aspon 1/8 velikosti obrazku a mezery maximalne tak velike

    vector<Vec4i> lines;
    HoughLinesP(edges, lines, 1, 2 * CV_PI / 180, 15, line_threshold, line_threshold);				// najde nam vsechny cary

    vector<int> labels;
    label_count = partition(lines, labels, bind(&is_equal, _1, _2, vertical));						// zmena poctu aktivnich parametru			
    vector<line_info> result;
    for (uint32_t i(0); i < lines.size(); ++i) {
        
        result.emplace_back(create_line_info(cv::Point{ lines[i][0], lines[i][1] }, Point{ lines[i][2], lines[i][3] }, labels[i]));			//vytvorime objekty line_info , ktere obsahuje informace o carach
        if (vertical && (result.back().angle > 90.0))
            result.back().angle -= 180;													// uprava uhlu, diky vypoctu byl nekdy o 180 stupnu jinak  a misto distribude takto: ____/\____, by byl \________/ a to pak kdyz vezmes prumer, tak dostanes picovinu!
    
    }
		

		
		
    return result;
}

// ============================================================================

vector<line_info> find_bounds(Mat const& edges, bool vertical)
{
    using namespace std::placeholders;



    Mat reduced_edges(edges.clone());

    Mat kernel;
    if (vertical) {
        kernel = getStructuringElement( MORPH_RECT, Size(1, 7));					// zbavime se car ve spatne orientaci  -> pomoci kernelu otoceneho patricnym smerem a opening
    } else {
        kernel = getStructuringElement( MORPH_RECT, Size(7, 1));
    }
    filter_mask(reduced_edges, kernel, { MORPH_OPEN});

    int32_t label_count(0);
    vector<line_info> lines(find_edge_lines(reduced_edges, label_count, vertical));
    
    
    
    
    vector<Scalar> colors(make_random_colors(label_count));
    vector<line_info> reduced_lines((reduce_edge_lines(lines, label_count)));
    
	sort_edge_lines(reduced_lines, vertical);													// sestrideni car

    Mat output_all;
    cvtColor(reduced_edges, output_all, COLOR_GRAY2BGR);
    for (uint32_t i(0); i < lines.size(); ++i) {
        line_info const& l(lines[i]);
        line(output_all, l.point[0], l.point[1], colors[l.label], 1, CV_AA);
        circle(output_all, l.center, 3, colors[l.label], -1);								
        
    }    

    Mat output_reduced;
    cvtColor(edges, output_reduced, COLOR_GRAY2BGR);
    for (int32_t i(0); i < reduced_lines.size(); ++i) {
				
        line_info const& l(reduced_lines[i]);
        line(output_reduced, l.point[0], l.point[1], colors[l.label], 1, CV_AA);
        circle(output_reduced, l.center, 3, colors[l.label], -1);
    }
    

    
	
    return reduced_lines;
}

// ============================================================================

void refine_plate_matches(vector<plate_match>& matches, cv::Size const& image_size)
{
    for (uint32_t i(0); i < matches.size(); ++i) {
        plate_match& match(matches[i]);					
        
        Mat work_image(Mat::zeros(image_size, CV_8UC1));						// udelame si prazdnou masku 
        fillPoly(work_image, match.contour_, Scalar(255));						// vykreslime do ni konturu

        Mat kernel( getStructuringElement( MORPH_RECT, Size(11, 3)));			// zase udelame opening a closing		11x3   protoze znacka je hodne siroka , tak je ten kernel hodně roztáhlý, díry jsou většinou vertikální a chceme jich co nejvic zaplnit
        Mat filtered_image(work_image.clone());
        filter_mask(filtered_image, kernel, { MORPH_CLOSE, MORPH_OPEN});		

        vector<point_vector> contours;
        findContours(filtered_image, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);		// zase najdeme konturu a preskreslime to, zbavime se zbytku der, ktere zustaly uvnitr kontury, ale uz jsou oddeleny od vnejsku
        drawContours(filtered_image, contours, -1, Scalar(255), -1);
        
        

        Mat edges;
        Canny(filtered_image, edges, 50, 200, 3);								// najde hrany nasi kontury a pomoci dilate roztahne na trosku tlusci hrany
        
        
        
        kernel = getStructuringElement( MORPH_ELLIPSE, Size(3, 3));
        filter_mask(edges, kernel, { MORPH_DILATE});	
					
		vector<line_info> lines_v(find_bounds(edges, true));				// detekce vertikalnich okraju
        vector<line_info> lines_h(find_bounds(edges, false));				// -||- horizontalnich

		

        refine_plate_match(match, edges, lines_v, lines_h);						// dostane na vstup puvodni match objekt a bere ho jako referenci kterou bude menit, masku okraje a dva zredukovany seznamy car
    }																			
}

// ============================================================================

Mat make_plate_matching_mask(Mat const& input_image)
{
    Mat hsv_image;
    cvtColor(input_image, hsv_image, COLOR_BGR2HSV);							// prevedeme si BGR do HSV - oddělíme informace o barvě od imformace o intenzite -> hue, saturation, value

    enum { HUE = 0, SAT = 1, VAL = 2 };											// protoze znacka 
    vector<Mat> h_s_v(3);
    split(hsv_image, h_s_v);													// rozdeleni do pole matic				

    Mat normalized_image;
    normalizer_->apply(h_s_v[VAL], normalized_image);							// normalizace aby jsme mohli pouzit fixni treshold

    Mat mask_gray;
    threshold(normalized_image, mask_gray, 127, 255, THRESH_BINARY + (false ? THRESH_OTSU : 0));			// threshold, use Otsu false    // saturovany mista

    Mat mask_saturation;
    threshold(h_s_v[SAT], mask_saturation, 48, 255, THRESH_BINARY_INV);			// chci odchytit nesaturovany mista  --  dve masky, jedna obsahuje mista, ktere jsou hodne svetla a jedna co maji malo barvy

    Mat mask;
    bitwise_and(mask_gray, mask_saturation, mask);								// mame masku svetlych malo barevnych mist

    return mask;
}

// ============================================================================

vector<plate_match>find_plate_matches(cv::Mat const& input_image)															// cilem je v teto funkci najit vsechna mista v obrazku, kde by mohla byt znacka
{
    Mat mask(make_plate_matching_mask(input_image));		
    
    
  																// slouzi pro vytvoreni masky
    
    if (true) {																// filter contour mask			// trochu si masku vyfiltruje od bordelu
        Mat kernel;
        kernel = getStructuringElement( MORPH_RECT, Size(3, 3));																// closing a opening 3x3 s obdelnikem, aby se zatahly dirky a spojily hodne blizke kontury
        filter_mask(mask, kernel, { MORPH_CLOSE, MORPH_OPEN} );				// napred MORPH_CLOSE, potom MORPH_OPEN
        kernel = getStructuringElement( MORPH_RECT, Size(9, 1));																// kernel 9x1 - siroky prouzek - zatahnou se diry co zpusobuji TODO
        filter_mask(mask, kernel, { MORPH_CLOSE, MORPH_OPEN} );
    }

	double image_area(input_image.rows * input_image.cols);											// vypocitame si plochu obrazku (pocet pixelu)
    Point image_center(input_image.cols / 2, input_image.rows / 2);									// souradnice prostredku
    double const DISTANCE_THRESHOLD(input_image.rows * (1/6.0));			// box_distance_pct_	// limit max. povolene vzdalenosti stredu znacky od stredu obrazku --> diky orezu je vetsinou znacka nekde uprostred

	vector<point_vector> contours;
    findContours(mask, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);			// najdeme kontury v masce
	
	vector<plate_match>  result;
	
	  
	
    
    for (int i(0); i < contours.size(); ++i) {
        
        plate_match match = create_plate_match(contours[i]);

        double box_fill_ratio(((contourArea(match.contour_[0]))) / (match.bounding_box_.size.area()));		// nekolik kriterii k vybrani pravdepodobne spz -> jelikoz je to obdelnik, tak ta kontura vyplni velkou cast sveho bound. boxu
		double image_fill_ratio((match.bounding_box_.size.area()) / image_area);
        double center_distance(get_distance(image_center, match.bounding_box_.center));						// kontrola na vzdalenost od stredu
       
        bool accepted(true);
        
        
		if(match.roi_.x < 0){
            accepted = false;
		}
		if(match.roi_.y < 0){
			accepted = false;
		}
        if(match.roi_.br().x > input_image.cols){
			accepted = false;
        }
        
        if (box_fill_ratio < 0.5)																			// min box fill ratio
            accepted = false;
        else if ((image_fill_ratio) < 0.15 && (image_fill_ratio > 0.4)) 									// min image fill ratio		
            accepted = false;
        else if (center_distance > DISTANCE_THRESHOLD) 
            accepted = false;
        else if (image_fill_ratio > 0.5) {																	// ochrana proti pretykani pres frame
            accepted = false;
        }

        if (accepted){
			 result.emplace_back(match);       

		}
    }
    
   

    return result;
}

// ============================================================================

extracted_plate extract(Mat const& source_image){									// extracted_plate
	
    Mat resized_image;
    resize(source_image, resized_image, cv::Size(), 3.0, 3.0, cv::INTER_CUBIC);		// scale factor, resize metoda - zvetsime, protoze pocatecni rozliseni je moc male a velmi lehce by došlo ke slévání
	vector<plate_match> plate_matches(find_plate_matches(resized_image));	   	   	// mame fleky, obcas neco precniva, neco je vykousnute	
	
    refine_plate_matches(plate_matches, resized_image.size());						// zlepseni
    
    if (plate_matches.size() != 1){
			
		Mat image_zero;
		
		extracted_plate result;
		result.image = image_zero;
			
		return result;
	}
	else{
		extracted_plate result(extract_match(plate_matches[0], resized_image));
		return result;
	} 	
}

// ============================================================================
	
extracted_plate ext(Mat img_segment){
	
	if(!(img_segment.empty())){
		extracted_plate result(extract(img_segment));
	return result;
	}
		
}
