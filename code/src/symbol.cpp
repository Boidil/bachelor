
#include "symbol.h"

list<pair<int,string>>
list_id_spz;

// ============================================================================
void make_combinations(std::vector<cut_sequence>& results
    , cut_sequence& current
    , int32_t n, int32_t k, int32_t dir
    , int32_t start_n, int32_t depth)
{
    for (int32_t i(start_n); i < n; ++i) {
        current.indices[depth] = (dir < 0) ? (n - 1 - i) : i;

        if (depth == (k - 1)) {
            results.push_back(current);
        } else {
            make_combinations(results, current, n, k, dir, i + 1, depth + 1);
        }
    }
}

// ----------------------------------------------------------------------------
std::vector<cut_sequence> make_combinations(int32_t n, int32_t k, int32_t dir)
{
    std::vector<cut_sequence> results;

    cut_sequence current;
    current.indices.resize(k);
    current.q = 0.0f;

    make_combinations(results, current, n, k, dir, 0, 0);

    return results;
}

// ----------------------------------------------------------------------------

cv::Mat derivative(cv::Mat const& curve, int32_t n)					// najde n-tou derivaci 1D krivky
{
    cv::Mat kernel(cv::Size(3, 1), CV_32FC1);
    kernel.at<float>(0, 0) = -0.5f;
    kernel.at<float>(0, 1) = 0.0f;
    kernel.at<float>(0, 2) = 0.5f;

    cv::Mat result(curve);
    for (int32_t i(0); i < n; ++i) {
        cv::filter2D(result, result, CV_32F, kernel);
    }
    return result;
}

// ----------------------------------------------------------------------------

int32_t find_best_edge(slice_vector const& slices, int32_t section_width, double scale, int32_t origin){				// vyberem rez, který je s nejvetsi pravdepodobnosti symbol
	
    int32_t i_best = -1;
    double max_q = 0.0;
    for (int32_t i(0); i < slices.size(); ++i) {
        double fractional_width(slices[i].width / static_cast<double>(section_width));									//  najde zlomek sirky
        double edge_distance((origin - slices[i].center) / static_cast<double>(section_width));							// vzdalenost od okraje

        double scaled_edge_distance(edge_distance * scale);																// vynasobi je bud 3,5 nebo 4,5, podle leve nebo prave strane
        scaled_edge_distance = 1.0 - scaled_edge_distance * scaled_edge_distance;										// vezmeme 1.0 - scaled_distance na druhou - zluta parabola

        double q(fractional_width * scaled_edge_distance);
        if (q >= max_q) {																								// vynasobime a vezmeme tu s nevyssi vahou
            i_best = i;
            max_q = q;
        }
    }
    return i_best;
}

// ----------------------------------------------------------------------------

plate_slicing find_slice_sides(cv::Mat const& image, plate_slicing const& slicing){					// ucelem je redukovat pocet rezu nalevo a napravo na jeden

    int32_t left_right(slicing.center.center - slicing.center.width / 2);
    int32_t left_width(left_right);
    int32_t i_left = find_best_edge(slicing.left, left_width, 3.5, 0);								// leva cast


    int32_t right_left = slicing.center.center - slicing.center.width / 2;
    int32_t right_width = image.cols - right_left;
    int32_t i_right = find_best_edge(slicing.right, right_width, 4.5, image.cols);					// prava cast


    plate_slicing new_slicing;																		// vytvorime novy plate_slicing, ktery ma na kazde strane jeden a vracime se zpatky
    if (i_left >= 0) {
        new_slicing.left.push_back(slicing.left[i_left]);
    } else {
        // Nenalezeny nejlepsi rezy 
        new_slicing.left.push_back({ 0, 9 });
    }
    new_slicing.center = slicing.center;
    if (i_right >= 0) {
        new_slicing.right.push_back(slicing.right[i_right]);
    } else {
        // Nenalezeny nejlepsi rezy
        new_slicing.right.push_back({ image.cols - 1, 9 });
    }

    return new_slicing;
}

// ----------------------------------------------------------------------------

void filter_mask(cv::Mat const& mask, cv::Mat const& kernel, int op)
{
    cv::morphologyEx(mask, mask, op, kernel);
}

// ----------------------------------------------------------------------------

plate_slicing split_plate_level_1(cv::Mat const& image)														// Prvni level separace na levou a pravou stranu, dostanu na leve strane 3 a na prave 4 symboly
{
	
	
    // Vertikalni projekce																					// Pomocí vertikální projekce vypočítáme průměrnou velikost pixelu
    cv::Mat column_sum;
    cv::reduce(image, column_sum, 0, cv::REDUCE_SUM);
    column_sum = 1 - (column_sum / image.rows);																// spocitame prumernou hodnotu,    bila je 1.0, cerna je 0.0 ,   znaky jsou cerne, invertujeme, znaky maji vysokou hodnotu a mezery nizke

    // Potlaceni uzkych mezer pomoci dilate
    cv::Mat kernel(cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 1)));
    filter_mask(column_sum, kernel, cv::MORPH_DILATE);														// pouzivame jen na jedne radce, proto ma smysl je sirka
																											
	
    double min_sum(0.0), max_sum(0.0);
    cv::minMaxLoc(column_sum, &min_sum, &max_sum);															// zjistime min. max. hodnotu krivky

    double mean_sum(cv::mean(column_sum)[0]);																// prumernou hodnotu krivky
    double thresh(min_sum + (mean_sum - min_sum) / 2);														// rozdil min. a prumeru

    cv::Mat above_thresh(column_sum > thresh);																// najdeme mista, ktera jsou vyssi nez treshold --> ZNAKY | vysledek bude bud nula nebo pozitivni hodnota

    // Najdu kde pozitivni hodnoty zacinaji a konci
    cv::Mat kernel_2(cv::Size(2, 1), CV_16SC1);																// najdeme mista, kde sekce spozitivni hodnotou zacinaji a konci
    kernel_2.at<int16_t>(0, 0) = -1;
    kernel_2.at<int16_t>(0, 1) = 1;

    cv::Mat edges;
    cv::filter2D(above_thresh, edges, CV_16S, kernel_2);													// filter se aplikuje na kazdou pozici. priklad 0,1 -> 1 | 1,0 -> -1 | 11 00 -> 0

    // Extrahuji nenulove hodnoty
    std::vector<cv::Point> edge_points;
    cv::findNonZero((edges != 0), edge_points);																// vytahneme si souradnice mist, nenulovych hodnot

    if (edge_points.empty()) {
        cout << "No edges" << endl;
    }

    slice_vector result;

    int i(0);																								// najdeme vsechny diry \_/ mista, kde je nalevo negativni slope a napravo positivni
    if (edges.at<int16_t>(edge_points[i].x) > 0) {															// edge obsahuje pozice vsech mist, kde se meni slope
        int left(0), right(edge_points[i].x);
        result.push_back({ (left + right) / 2, (right - left) | 1 });													
        ++i;
    }
    for (; i < edge_points.size() - 1; i += 2) {															// for bere vzdy situaci jako \_/ -- zjistujeme udoli
        int left(edge_points[i].x), right(edge_points[i + 1].x);
        result.push_back({ (left + right) / 2, (right - left) | 1 });
    }
    if (i < edge_points.size()) {
        int left(edge_points[i].x), right(image.cols - 1);
        result.push_back({ (left + right) / 2, (right - left) | 1 });
        ++i;
    }

    int32_t i_center(-1);																							// najit prostredni rez
    double max_q(0.0);
    for (int32_t i(0); i < result.size(); ++i) {
        double fractional_width(result[i].width / static_cast<double>(image.cols));									//  zlomek sirky
        double center_distance(1.0 - abs(image.cols - 2 * result[i].center) / static_cast<double>(image.cols));		// odchylka je normalizovana do rozsahu 0-1
        double q(fractional_width * center_distance);																// vynasobime je a dostaneme vahu, cim vyssi tim pravdepodobnejsi, ze se jedna o diru ve stredu
        if (q >= max_q) {
            i_center = i;
            max_q = q;
        }
    }

    if (i_center == 0) {
        // najdu bod rezu nejvic vlevo
        result.insert(result.begin(), { 0, 9 });
        ++i_center;
    }
    if (i_center == result.size() - 1) {
        // A nejvice vpravo
        result.push_back({ image.cols - 1, 9 });
    }

    plate_slicing slicing;
    slicing.left.assign(result.begin(), result.begin() + i_center);
    slicing.center = result[i_center];
    slicing.right.assign(result.begin() + i_center + 1, result.end());
    return slicing;
}

// ----------------------------------------------------------------------------

section_slicing split_plate_level_2(cv::Mat const& image)
{
    section_slicing result;

    // Vertikalni prokce
    cv::reduce(image, result.column_value, 0, cv::REDUCE_SUM);							
    result.column_value = 1 - (result.column_value / float(image.rows));

    // Vypocteni druhe derivace vertical projection křivky
    cv::Mat d2_column_value(derivative(result.column_value.clone(), 2));								// zmena sklonu - kdyz je krivka strmejsi, je kladny, kdyz strmost klesa, je zaporny

    // rozdělit derivativu dle puvodni krivky
    result.d2_ratio = d2_column_value / (result.column_value + 1);										// deriv/sum 

    cv::blur(result.d2_ratio, result.d2_ratio, cv::Size(3, 1));											//trochu vyhladime a normalizujeme (0,1)
    cv::normalize(result.d2_ratio, result.d2_ratio, 1.0, 0.0, cv::NORM_MINMAX);

    // najdeme lokalni minima a maxima
    cv::Mat kernel(cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 1)));
    cv::Mat d_src, e_src;
    cv::dilate(result.d2_ratio, d_src, kernel);
    cv::erode(result.d2_ratio, e_src, kernel);

    d_src = (result.d2_ratio >= d_src);
    e_src = (result.d2_ratio > e_src);

    cv::Mat maxima(d_src & e_src);																		// najdeme lokalni maxima(tluste) a minima(carkovane)
    cv::Mat minima(~(d_src | e_src));

    // minima a maxima jsou nenulove hodnoty pixelu v krivce matice
    // prendame je do seznamu x
    std::vector<cv::Point> minima_pos, maxima_pos;													// pak jejich pozice (indexy sloupcu)
    cv::findNonZero(minima, minima_pos);
    cv::findNonZero(maxima, maxima_pos);

    for (auto const& p : minima_pos) {
        result.minima.push_back(p.x);
    }
    for (auto const& p : maxima_pos) {
        result.maxima.push_back(p.x);
    }

    if (result.minima.size() <= result.maxima.size()) {					// overeni aby bylo maximum, ktere ma nakde strane minimum, 
        if (result.maxima.front() == 0) {
            result.maxima.erase(result.maxima.begin());
        }
        if (result.maxima.back() == (image.cols - 1)) {
            result.maxima.erase(result.maxima.end() - 1);
        }
    }
    
    if (result.minima.size() == result.maxima.size()) {					// minima a maxima stejne
		return section_slicing();
	}


    // Vypocet vahy pro kazdy rez										
    for (int32_t i(0); i < result.maxima.size(); ++i) {										// prochazime vsechny maxima - potencialni body k rezu
      
        int32_t l(result.minima[i]);
        int32_t c(result.maxima[i]);														// najdeme pozici maxima (bodu rezu) a jeho sousednich minim (l,r)
        int32_t r(result.minima[i + 1]);

        // Derivace hodnot pro vsechny pozice rezu
        float vl_d(result.d2_ratio.at<float>(l));											// vytahneme hodnoty krivky v tech bodech
        float vc_d(result.d2_ratio.at<float>(c));
        float vr_d(result.d2_ratio.at<float>(r));

        // Váhový faktor -- preferujeme největší rozdíl mezi vrcholy a údolí
        float q_1((2.0f * vc_d - (vl_d + vr_d)) / (2.0f - vc_d));					 		// soucet absolutniho rozdilu mezi maximem a minimem --> cim vyssi vrsek a nizsi sousedni udoli, tim vyssi hodnota q

        float vl_v(result.column_value.at<float>(l));								
        float vc_v(result.column_value.at<float>(c));
        float vr_v(result.column_value.at<float>(r));

        float q_2((-2.0f * vc_v + vl_v + vr_v) / ((0.01f + vc_v * vc_v) * (r - l)));		// (-2.0f * vc_v + vl_v + vr_v)  -2 * (leva + prava + stred) cervene krivky         , (r - l) - pocet sloupcu od leveho k do praveho minima, (0.01f + vc_v * vc_v) - hodnota cervene v maximu, +  0.01f kdyby byla nula
																							// vc_v na vc_v meni pomer
        result.weights.push_back(q_1 * q_2);
    }

    return result;
}

// ----------------------------------------------------------------------------
std::vector<cut_sequence> split_plate_level_3(section_slicing const& cuts
    , float scale, int32_t dir)
{
	
    // Expected width of a symbol and the tolerance in its position
    float exp_symbol_width((cuts.maxima.back() - cuts.maxima.front()) / scale);
    float exp_symbol_tol(exp_symbol_width * 0.5f);

    // Vytvorim matici vsech paru rezu
    // Vaha predstavuje pravdepodobnost, ze par rezu obsahuje symbol
    std::vector<std::vector<cut_info>> cut_matrix;
    for (uint32_t start_i(0); start_i < cuts.maxima.size(); ++start_i) {
        int32_t start_pos(cuts.maxima[start_i]);

        std::vector<cut_info> cut_row;
        for (uint32_t i(0); i < cuts.maxima.size(); ++i) {
            int32_t distance(cuts.maxima[i] - start_pos);
            float corrected_distance(distance +
                ((i < start_i) ? exp_symbol_width : -exp_symbol_width));
            float tolerance_fraction(corrected_distance / exp_symbol_tol);

            float weight(std::max(0.0f, 1.0f - tolerance_fraction * tolerance_fraction));

            cut_info info;
            info.position = cuts.maxima[i];
            info.distance_fraction = distance / static_cast<float>(cuts.column_value.cols);
            info.q = weight * cuts.weights[i];
            cut_row.push_back(info);
        }

        cut_matrix.push_back(cut_row);
    }

    // Vytvorime vsechny mozne kombinace rezu
    int32_t cut_count((dir < 0) ? 4 : 5);
    std::vector<cut_sequence> sequences(make_combinations(
        static_cast<int32_t>(cuts.maxima.size()), cut_count, dir));

    // Spocitame vahy vsech hodnot
    cv::Mat mean_value, std_value, widths(cv::Size(cut_count - 1, 1), CV_32FC1);
    for (auto& sequence : sequences) {
        float total_q(0.0f), total_width(0.0f);
        for (int32_t i(0); i < cut_count - 1; ++i) {
            int32_t start_i(sequence.indices[i]), end_i(sequence.indices[i + 1]);
            total_q += cut_matrix[start_i][end_i].q;
            float correct_width(cut_matrix[start_i][end_i].distance_fraction * dir);
            total_width += correct_width;
            widths.at<float>(i) = correct_width;
        }

        cv::meanStdDev(widths, mean_value, std_value);
        float std_factor(1.01f - static_cast<float>(std_value.at<double>(0)));

        sequence.q = total_q * std::sqrt(total_width) * std_factor;
    }
   								
    if(sequences.size() <= 5){								// rezu prvku je mene nez 5
		std::sort(begin(sequences), end(sequences)
			, [](cut_sequence const& a, cut_sequence const& b) -> bool
			{
				return a.q > b.q;
			});
	}
	else{
		partial_sort(begin(sequences), begin(sequences)+5, end(sequences), [](cut_sequence const& a, cut_sequence const& b) -> bool			
		{		
			return a.q > b.q;					
		});
	}


    return sequences;
}

// ============================================================================

plate_segmentation do_segmentation(cv::Mat const& image, Ptr<ml::KNearest> knn[], int ID)
{
   
    cv::Mat image_v_blur, image_vh_blur;
    cv::blur(image, image_v_blur, cv::Size(1, 31));												// rozmazeme znaky, abychom meli jednu skupinu vlevo, sirokou mezeru a jednu skupinu vpravo
    cv::blur(image_v_blur, image_vh_blur, cv::Size(15, 1));
   
	
    plate_slicing slicing_1 = split_plate_level_1(image_vh_blur);				
    slicing_1 = find_slice_sides(image_vh_blur, slicing_1);


    std::pair<int32_t, int32_t> left_bounds(										
        slicing_1.left[0].center + slicing_1.left[0].width / 2									// mame stred + polovinu jeho sirky								-- carkovana cara napravo od tluste v grafu
        , slicing_1.center.center - slicing_1.center.width / 2);								// pozice stredniho rezu - polovina jeho sirky					-- carkovana cara nalevo od tluste v grafu
																								// dostaneme cast kde jsou 3 znaky
	
    if (left_bounds.first >= left_bounds.second) {												// levy okraj musi mit mensi index nez pravy, kontrola kdyby nastala chyba
		return plate_segmentation();
	}
	
    section_slicing slicing_2_1 = split_plate_level_2(image_vh_blur.colRange(					// vvybereme cast obrazku, ktera odpovida identifikovanemu rozsahu - levou
        left_bounds.first, left_bounds.second));

	if(slicing_2_1.minima.empty()) {
		return plate_segmentation();
	}

    std::vector<cut_sequence> slicing_3_1 = split_plate_level_3(slicing_2_1, 3.5, -1);

    std::pair<int32_t, int32_t> right_bounds(
        slicing_1.center.center + slicing_1.center.width / 2
        , slicing_1.right[0].center - slicing_1.right[0].width / 2);

	 if (right_bounds.first >= right_bounds.second) {
		return plate_segmentation();
	}

    section_slicing slicing_2_2 = split_plate_level_2(image_vh_blur.colRange(
        right_bounds.first, right_bounds.second));  

	if (slicing_2_2.minima.empty()) {
		return plate_segmentation();
	}

	if(slicing_2_2.maxima.size() > 20){
		return plate_segmentation();
	}

    if (slicing_2_2.minima.empty() || slicing_2_2.maxima.empty()) {							// chybovy vyskok
        return plate_segmentation();
    }
																							// kontrola, jestli pocet rezu neni moc vysoka
	std::vector<cut_sequence> slicing_3_2 = split_plate_level_3(slicing_2_2, 4.5, 1);
   
    if(slicing_3_2.empty() || slicing_3_1.empty()){											// chybovy vyskok
		return plate_segmentation();
	}
	
	int ATTEMPT_COUNT;
	if ((slicing_3_1.size() < 5) || (slicing_3_2.size() < 5)) {
		ATTEMPT_COUNT = std::min(slicing_3_1.size(), slicing_3_2.size());  
	}
	else{
		ATTEMPT_COUNT = 5;												// 5 variant oriznuti znaku	
	}

	if ((slicing_3_1.size() == 0) || (slicing_3_2.size() == 0)) {
		return plate_segmentation();
	}

    plate_segmentation segmentation;
    segmentation.symbols.resize(7); 
    
    string spz;
    int digitoralpha = 0;
    string char_plate;	
    
    for (auto& symbols : segmentation.symbols) {
        symbols.resize(ATTEMPT_COUNT);
    }       
		int q = 1;
		int j = 0;
		
		string spz_plate;
		
		for (int32_t j(0); j < 3; ++j) {								// tri pozice znaku nalevo
			ofstream out("tmp");	
			
			for (int32_t i(0); i < ATTEMPT_COUNT; ++i) {
			
			
            // Musim se vratit zpet, indexuji od stredu
            int32_t first_index(slicing_3_1[i].indices[3 - j]);
            int32_t last_index(slicing_3_1[i].indices[2 - j]);

            int32_t start_column(slicing_2_1.maxima[first_index]);
            int32_t end_column(slicing_2_1.maxima[last_index]);

            plate_symbol_info& info(segmentation.symbols[j][i]);
            info.bounding_box = cv::Rect(start_column + left_bounds.first, 0, end_column - start_column, image.rows);

// ---------------------------------------------------------------------
			Mat tmp_image = image(info.bounding_box);

				if(((image(info.bounding_box).cols >= 25)) && (image(info.bounding_box).cols <= 53)){									// nastavit horni hranici
					spz = precognition(tmp_image, j, digitoralpha, char_plate, knn);	
					if(spz != "empty"){
						out << spz << endl;	
					}
				}
				
					if( i == (ATTEMPT_COUNT-1) ){									// posledni pruchod
						out.close();
						char_plate = symbol();
						spz_plate.append(char_plate);								// vypise do stringu znaky za sebou
						
						if((j == 0) && (isdigit(char_plate[0]))){
							digitoralpha = 1;										// cislo
						}
						else if((j == 0) && (isalpha(char_plate[0]))){
							digitoralpha = 2;										// znak
						}	
					}	
			}
			q++;
		}
			
	string spz_plate2;
	int tmp_j = 3;
	
	for (int32_t j(0); j < 4; ++j) {											// ctyri pozice znaku napravo
		ofstream out("tmp");	
		
		for (int32_t i(0); i < ATTEMPT_COUNT; ++i) {

			int32_t offset(static_cast<int32_t>(slicing_3_1[i].indices.size()) - 1);
        
            int32_t first_index(slicing_3_2[i].indices[j]);
            int32_t last_index(slicing_3_2[i].indices[j + 1]);

            int32_t start_column(slicing_2_2.maxima[first_index]);
            int32_t end_column(slicing_2_2.maxima[last_index]);

            plate_symbol_info& info(segmentation.symbols[j + offset][i]);
            info.bounding_box = cv::Rect(start_column + right_bounds.first
                , 0
                , end_column - start_column
                , image.rows);

			Mat tmp_image = image(info.bounding_box);
        
			if(((image(info.bounding_box).cols >= 25)) && (image(info.bounding_box).cols <= 53)){					// vymezeni velikost boxu rozrezane spz
					spz = precognition(tmp_image, tmp_j, digitoralpha=0, char_plate, knn);							// nastaveni na posledni 4 cislice
					
					if(spz != "empty"){
						out << spz << endl;	
					}	
			}
        
			if( i == (ATTEMPT_COUNT-1) ){									// posledni pruchod
				out.close();
				char_plate = symbol();
				spz_plate2.append(char_plate);								// vypise do stringu znaky za sebou
							
			}	
        }
        tmp_j++;
        
    }

    spz_plate.append(spz_plate2);
    
    if(spz_plate.length()  == 7){
		list_id_spz.push_back(pair<int,string>(ID,spz_plate));
	}

    return segmentation;
}

// ============================================================================

plate_segmentation extract_symbols(cv::Mat const& image, Ptr<ml::KNearest> knn[], int ID)           
{

    Mat resized_image;								
    double scale(static_cast<double>(80) / image.rows);												// uprava velikosti vysky na stejno, kvuli perspektivnimu zkresleni
    resize(image, resized_image, Size(), scale, scale, INTER_CUBIC);																			

    return do_segmentation(resized_image, knn, ID);
    
}

// ============================================================================

int extract_symbols(cv::Mat const& image, cv::Mat const& mask, Ptr<ml::KNearest> knn[], int ID){
    
    if(image.empty()){
		return 1;	
	}
    
    if(mask.empty()){
		return 1;
	}
	
	if(image.size() != mask.size()){
		return 1;
	}
    
    cv::Mat work_mask;
    mask.convertTo(work_mask, CV_8UC1, 255);	
		
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(work_mask, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    
    if (contours.size() != 1) {
		cout << "Unexpected contour count." << endl;
		return 1;
    }
    
    cv::Rect bbox(cv::boundingRect(contours[0]));
    cv::Mat cropped_image(image(bbox));
    extract_symbols(cropped_image, knn, ID);
    
    return 0;
}
