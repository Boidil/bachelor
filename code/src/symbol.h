#ifndef SYMBOL_H
#define SYMBOL_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/types.hpp>
#include <cmath>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>
#include <string>
#include <list>
#include <utility>
#include "precognition.h"
#include "choose_symbol.h"

// =====================================================================

using namespace std;
using namespace cv;

// =====================================================================

double const BORDER(0.01);
double const SCALES(1.5);

struct slice_info
{
    int32_t center;
    int32_t width;
};

// =====================================================================

typedef std::vector<slice_info> slice_vector;

struct section_slicing
{
    std::vector<int32_t> minima;
    std::vector<int32_t> maxima;
    cv::Mat column_value;
    cv::Mat d2_ratio;
    std::vector<float> weights;
};

struct plate_slicing
{
    slice_vector left;			// vektor s min. jednim 
    slice_info center;			// jeden rez pro prostredek
    slice_vector right;
};

struct plate_symbol_info
{
    cv::Point2i position() const;
    double area() const;

    cv::Rect bounding_box;

    cv::Mat raw_image;
    cv::Mat image;
};

typedef std::vector<plate_symbol_info> symbol_info_list;

struct plate_segmentation
{
    std::vector<symbol_info_list> symbols;
};

struct cut_sequence
{
    std::vector<int32_t> indices;
    float q;  
};

struct cut_info
{
    int32_t position;
    float distance_fraction;
    float q;
};

// =====================================================================

extern list<pair<int, string>>
list_id_spz;

// =====================================================================

int extract_symbols( cv::Mat const& image, cv::Mat const& mask, Ptr<ml::KNearest> knn[], int);

#endif
