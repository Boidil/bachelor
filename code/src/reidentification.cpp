#include "reidentification.h"

string reidentification(string actual_spz, vector<string>& list_spz){
	
	unsigned i = 0;
	string result;
	
	while(i < list_spz.size()){	
		string target_plate = list_spz.at( i );
		
		if(target_plate == actual_spz){
			result = actual_spz;					
			list_spz.erase(list_spz.begin() + i);		// smaze reidentifikovanou SPZ z vektoru
		}
		else{
			i++;										// prejde na dalsi spz v seznamu
		}
	}
    return result;
}

