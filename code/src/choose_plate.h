#ifndef CHOOSE_PLATE_H
#define CHOOSE_PLATE_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <list>

// ====================================================================

using namespace std;
using namespace cv;

// ====================================================================

struct chars_p{
	
	int count = 0;
	int weight = 0;
	string plate_chars;
	
};

struct final_char_p{
	
	int count = 0;
	int weight;
	string plate_chars;
	int test = 1;

};

// ====================================================================

string final_plate( list<string> );

#endif
