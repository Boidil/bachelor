
#include "main.h"

// ====================================================================

String window = "Detection car plates"; 

int main(int argc, char** argv){
	
	Ptr<ml::KNearest> knn[40](ml::KNearest::create());
	Ptr<ml::TrainData> data;
	
	// *** SADY NATRENOVANYCH ZNAKU ***

	// Prvni znak obou typu
	data =  ml::TrainData::loadFromCSV("./sets/0_both.csv",0); 	knn[0]->train(data);						
	// Druhy znak noveho typu
	data =  ml::TrainData::loadFromCSV("./sets/2_new.csv",0);	knn[1]->train(data);							
	// Treti znaky noveho typu
	data =  ml::TrainData::loadFromCSV("./sets/3_new_a.csv",0);	knn[2]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_b.csv",0);	knn[3]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_c.csv",0);	knn[4]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_e.csv",0);	knn[5]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_h.csv",0);	knn[6]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_j.csv",0);	knn[7]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_k.csv",0);	knn[8]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_l.csv",0);	knn[9]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_m.csv",0);	knn[10]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_p.csv",0);	knn[11]->train(data);		
	data =  ml::TrainData::loadFromCSV("./sets/3_new_s.csv",0);	knn[12]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_t.csv",0);	knn[13]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_u.csv",0);	knn[14]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/3_new_z.csv",0);	knn[15]->train(data);	
	// Druhy znak stareho typu
	data =  ml::TrainData::loadFromCSV("./sets/2_old_a.csv",0);	knn[16]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_b.csv",0);	knn[17]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_c.csv",0);	knn[18]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_d.csv",0);	knn[19]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_f.csv",0);	knn[20]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_h.csv",0);	knn[21]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_j.csv",0);	knn[22]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_k.csv",0);	knn[23]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_l.csv",0);	knn[24]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_m.csv",0);	knn[25]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_n.csv",0);	knn[26]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_o.csv",0);	knn[27]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_p.csv",0);	knn[28]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_r.csv",0);	knn[29]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_s.csv",0);	knn[30]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_t.csv",0);	knn[31]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_u.csv",0);	knn[32]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_v.csv",0);	knn[33]->train(data);	
	data =  ml::TrainData::loadFromCSV("./sets/2_old_z.csv",0);	knn[34]->train(data);	
	// Treti znak stareho typu
	data =  ml::TrainData::loadFromCSV("./sets/3_old.csv",0);	knn[35]->train(data);	
	// Cislice
	data =  ml::TrainData::loadFromCSV("./sets/numbers.csv",0);	knn[36]->train(data);	

// --------------------------------------------------------------------	
	
	namedWindow(window, 0); resizeWindow(window, 712,484);
	
	vector<string> list_spz;	
	string words;
	
	VideoCapture capture(argv[1]);

	// Prvni parametr je vzdy spustene video
	// Druhy parametr je vystupni soubor na SPZ
 	// Treti parametr muze byt vstupni textovy soubor s SPZ k reidentifikaci
	
	bool isReidentifikace;
	if(argc == 3){
			cout << "Byla spustena pouze detekce s identifikaci" << endl;
			isReidentifikace = false;
	}
	else if(argc == 4){
				
			ifstream wordfile(argv[3]);
			
			if(!wordfile.is_open()){
				cout << "Soubor s SPZ nelze nacist" << endl;
				return 1;
			}	
			cout << "Byla spustena reidentifikace" << endl;

			while (wordfile >> words){
					list_spz.push_back(words);
			}
			isReidentifikace = true;
			wordfile.close();			
	}
	else{
			cout << "Byl zvolen nespravny pocet parametru." << endl;
			cout << "Pokud chcete pouze rozpoznani, zvolte prvni parametr urcujici video, ktere chcete pouzit a vystupni soubor na rozpoznane SPZ." << endl;
			cout << "Pokud chcete reidentifikovat, pridejte jako treti parametr textovy soubor s SPZ k reidentifikaci." << endl;
			cout << "Priklad: ./Exe 01A.MTS output.txt znacky.txt" << endl;
			cout << "Vsechny rozpoznane SPZ, jak identifikovane, tak reidentifikovane budou ulozeny do souboru plates.txt" << endl;
			exit(1);
	}		
	
	if(!capture.isOpened()){
			cout << "Video nelze otevrit, zkuste zkontrolovat vstup!" << endl;
			return 1;	
	} 
	
	ofstream plates(argv[2]);
	if(!plates.is_open()){
		cout << "Chyba vystupniho souboru" << endl;
		return 1;
	}	
	Mat kernel(getStructuringElement(MORPH_RECT, Size(3, 3)));   
	
// --------------------------------------------------------------------	
	
	Mat	street_mask = imread("./background/reduce_mask1.png");														// nacteni masky pro odecteni od pozadi
	if(street_mask.empty()){
		cout << "MASKA POZADI NEBYLA NACTENA" << endl;
		exit(1);
	}
	resize(street_mask, street_mask, Size(), (1.0 / SCALE), (1.0 / SCALE), INTER_AREA);
	cvtColor(street_mask, street_mask, CV_BGR2GRAY);                          									// konverze snimku do odstinu sedi
	
// --------------------------------------------------------------------		
	
	// *** HLAVNI SMYCKA PROGRAMU ***
	int frames = 0;
	
	int xxx = 0; 
	double t = 0;
	
	Mat frame_original, frame_resize, frame_gray, frame_background; 
	
	for(;;){	
		capture.read(frame_original);																	// nacitani jednotlivych snimku z videa
		if(frame_original.empty()){																		// ukonceni prehravani pokud je prazdny snimek
			 cout << "KONEC PREHRAVANI" << endl;
			 plates.close();
			 remove("tmp");																				// pomocny soubor
			 return 0;
		}
		
		cvtColor(frame_original, frame_resize, CV_BGR2GRAY);                          					// konverze snimku do odstinu sedi
        resize(frame_resize, frame_gray, Size(), (1.0 / SCALE), (1.0 / SCALE), INTER_AREA);				// zmena rozliseni obrazku pro zrychleni programu
		
		if(frames == 0 || frames == 500){																// probiha kazdych 10s
            frame_background = background_substraction(frame_gray, frames);								// dynamicka zmena pozadi pro redukci drobnych zmen
			frames = 0; 	
        }
		
		Mat frame_diff = image_diff(frame_background, frame_gray, kernel, street_mask);																// odecteni pozadi, kvuli redukci oblasti ve ktere je detekovana spz
		if(!(frame_diff.empty())){
			detect_spz(frame_diff, frame_original, capture, knn, plates, list_spz, isReidentifikace);												// detekovani znacek
		}

		line(frame_original, Point(0,frame_original.rows/2) , Point(frame_original.cols,frame_original.rows/2), Scalar(100,205,255), 10, 8);		// cara v prostredku obrazu urcujici hranici detekce spz			
		imshow(window, frame_original);																												// zobrazeni jednotlivych snimku videa									
		
		frames++;
		waitKey(1);
	
	}
	return 0;
}
