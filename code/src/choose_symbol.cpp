#include "choose_symbol.h"

// --------------------------------------------------------------------------------------------------

// FUNKCE
final_char correct_char(ifstream& file_IN){

	chars znacky[5] = {};
	int x = 0;
	int weight = 0;
	string word;
	
	while (file_IN >> word){	
		int memory = 0;
		weight++;														// zvyseni vahy kazdeho prvku co prijde
		if(x == 0){														// prvni hodnota
			znacky[x].plate_chars = word;
			znacky[x].count++;
			znacky[x].weight = weight;	
		}
		else{			
			for(int i = 0; i < x; i++){									// prohledani pameti, jestli se prvek uz nevyskytl
					if(znacky[i].plate_chars == word){
						znacky[i].count++;	
						memory = 1;
					}
			}
			if(memory == 0){											// prvek se nevyskytl, tudiz je pridan na dalsi pozici
				znacky[x].plate_chars = word;
				znacky[x].count++;
				znacky[x].weight = weight;
			}
		}	
		if(memory == 0){												// v pripade pridani noveho prvku, iterace na dalsi pozici
			x++;
		}		
   	}
   	
   	final_char result;
   	for(int x = 0; x < 5; x++){											// vybrani nejcastejsiho prvku s nejnizsi vahou
	
		if(znacky[x].plate_chars.length() == 0){
			continue;	
		}
	
		if(result.count < znacky[x].count){
			result.plate_chars = znacky[x].plate_chars;
			result.count = znacky[x].count;
			result.weight = znacky[x].weight;
		}
		else if(result.count == znacky[x].count){
				if(result.weight > znacky[x].weight){
					result.plate_chars = znacky[x].plate_chars;
					result.count = znacky[x].count;
					result.weight = znacky[x].weight;
				}
		}
	}
	return result;
}

// --------------------------------------------------------------------------------------------------

// HLAVNI FUNKCE
string symbol() {

	ifstream file_IN("tmp");
	string symbol;
	final_char result;
    result = correct_char(file_IN);
	symbol = result.plate_chars;
	
    return symbol;
}

// --------------------------------------------------------------------------------------------------
