#ifndef REIDENTIFICATION_H
#define REIDENTIFICATION_H


#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;
using namespace cv;

string reidentification(string actual_spz, vector<string>& list_spz);


#endif
