#ifndef SEGMENT_H
#define SEGMENT_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <boost/format.hpp>
#include <cmath>
#include <sstream>
#include <opencv2/core/types.hpp>
#include <vector>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>

// =====================================================================

using namespace std;
using namespace cv;
						
// =====================================================================

typedef std::vector<cv::Point> point_vector;
typedef std::vector<point_vector> contour_vector;

typedef std::vector<cv::Point> point_vector;
typedef std::vector<point_vector> contour_vector;

// =====================================================================

struct extracted_plate
{
    cv::Mat image;
    cv::Mat mask;
};

struct plate_match{
	contour_vector contour_;
    cv::RotatedRect bounding_box_;
    contour_vector bounding_box_points_;
    contour_vector convex_hull_points_;
    cv::Rect roi_;
};

struct line_info
{
    Point point[2];
    Point2f center;
    double angle;
    int32_t label;
};

// =====================================================================

extracted_plate ext(Mat);


#endif
