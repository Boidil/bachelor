#ifndef PRECOGNITION_H
#define PRECOGNITION_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>


// ====================================================================

using namespace std;
using namespace cv;


// ====================================================================

typedef std::vector<std::string> string_list;
struct char_match_t2
{
    cv::Point2i position;
    cv::Mat image;
};
typedef std::vector<char_match_t2> char_match_list2;

// ===================================================================

string precognition(Mat, int, int, string, Ptr<ml::KNearest> knnx[]);

#endif

