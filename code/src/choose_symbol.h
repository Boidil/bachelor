#ifndef CHOOSE_SYMBOL_H
#define CHOOSE_SYMBOL_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>

// ====================================================================

using namespace std;
using namespace cv;

// ====================================================================

struct chars{
	
	int count = 0;
	int weight = 0;
	string plate_chars;
	
};

struct final_char{
	
	int count = 0;
	int weight;
	string plate_chars;

};

// ====================================================================

string symbol();

#endif
