
#include "background_different.h"

Mat image_diff(Mat frame_background, Mat frame_gray, Mat kernel, Mat street_mask){
	
	Mat frame_diff, drawing, stddev , mean;
	double stddevValue;

	absdiff(frame_background, frame_gray, frame_diff);											// odecteni pozadi	
	meanStdDev(frame_diff,mean,stddev); 														// stddev number of channels is 
	
    stddevValue= stddev.at<double>(0,0);	    
	vector<vector<Point> > contours;
	contour_vector convex_hull_points(1);
	
	
    if( stddevValue > 6 ){																		// odstraneni vlivu pozadi pri otsu prahovani, pri hodnote vyssi nez 6 do snimku vstupuje automobil
		threshold(frame_diff,frame_diff, 0, 255, CV_THRESH_OTSU);  
		bitwise_and(frame_diff, street_mask, frame_diff);		
		dilate(frame_diff, frame_diff, kernel, Point(-1, -1), 5);
		erode(frame_diff, frame_diff, kernel, Point(-1, -1), 4);

		findContours(frame_diff, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));		
		vector<RotatedRect> minRect(contours.size());
					
		Mat drawing = Mat::zeros( frame_diff.size(), CV_8UC1 );
			
		for( int i = 0; i< contours.size(); i++ ){
			minRect[i] = minAreaRect(contours[i]); 
			if(((minRect[i].size.width*minRect[i].size.height) < 40) || (minRect[i].size.width < 20) || (minRect[i].size.height < 20))			// pokud najde moc maly box, tak ho preskoci 
				continue;
				  
			convexHull(contours[i], convex_hull_points[0]);											// spojení okrajů kontur
			drawContours(drawing, convex_hull_points, -1, Scalar(255,255,255) , -1);				// vyplnění oblasti

		} 
		return drawing;
	}
	Mat zero_image;
	return zero_image;			// vrati prazdny snimek	
}
