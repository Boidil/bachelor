#include "precognition.h"

// ----------------------------------------------------------------------------

Mat kernel(getStructuringElement(MORPH_RECT, Size(3, 3)));

// ----------------------------------------------------------------------------

Mat segment_image(cv::Mat const& img)
{
	
	Mat image = img.clone()*255; 
	image.convertTo(image, CV_8UC1);

    resize(image, image, Size(), (4.0), (4.0), INTER_AREA);					// zvetseni znaku
	blur(image, image, Size(11,1));
	blur(image, image, Size(1,11));

	equalizeHist(image, image);												// vyrovnani histogramu
    threshold(image, image, 80, 255, THRESH_BINARY);              			// 70 - malo, 100 - moc
   	dilate(image, image, kernel, Point(-1, -1), 1);							// zvyrazeni kontur znaku
	erode(image, image, kernel, Point(-1, -1), 3);	
	
	Mat inverse_img;
	
    cv::bitwise_not(image, inverse_img);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;

    double const MIN_CONTOUR_AREA(10000.0);
    double const MAX_CONTOUR_AREA(1000000.0);

    cv::findContours(inverse_img, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	Mat ximage;
	
	 for (int i(0); i < contours.size(); i++) {
        cv::Rect bounding_box(cv::boundingRect(contours[i]));
        int bb_area(bounding_box.area());
        if ((bb_area >= MIN_CONTOUR_AREA) && (bb_area <= MAX_CONTOUR_AREA)) {
            ximage = image(bounding_box); 
        }
    }

	image = ximage.clone();

    return image;
}

// ----------------------------------------------------------------------------

string process_image2(Mat const& img, int j, int digitoralpha, string char_plate, Ptr<ml::KNearest> knnx[])
{
		
        Mat small_char, tmp, small_char_float;
        resize(img, small_char, Size(40, 40), 0, 0, INTER_LINEAR);			// zvetseni prichoziho znaku na velikost znaku s kterym je porovnavan

        small_char.convertTo(small_char_float, CV_32FC1);
        Mat small_char_linear(small_char_float.reshape(1, 1));				// konvertovani na jeden radek

        float p = 0;
        string plate;
        
        // --------- PRVNI ZNAK PRO STARY I NOVY TYP ------------------
        if(j == 0){
			p = knnx[0]->findNearest(small_char_linear, 1, tmp);		
		}
		// --------- DRUHY ZNAK PRO NOVY TYP --------------------------
		else if((j == 1) && (digitoralpha == 1)){
			p = knnx[1]->findNearest(small_char_linear, 1, tmp);	
			
		}
		// --------- TRETI ZNAK PRO NOVY TYP --------------------------
		
		if((j == 2) && (digitoralpha == 1)){		
			if(char_plate[0] == 'A'){																					// Hlavni mesto Praha
				p = knnx[2]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'B'){																				// Jihomoravsky (Brno)
				p = knnx[3]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'C'){																				// Jihocesky (Ceske Budejovice)
				p = knnx[4]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'E'){																				// Pardubicky (Pardubice)
				p = knnx[5]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'H'){																				// Kralovehradecky (Hradec Kralove)
				p = knnx[6]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'J'){																				// Vysocina (Jihlava)
				p = knnx[7]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'K'){																				// Karlovarsky (Karlovy Vary)
				p = knnx[8]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'L'){																				// Liberecky (Liberec)
				p = knnx[9]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'M'){																				// Olomoucky (Olomouc)
				p = knnx[10]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'P'){																				// Plzensky (Plzen)
				p = knnx[11]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'S'){																				// Stredocesky
				p = knnx[12]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'T'){																				// Moravskoslezsky (Ostrava)
				p = knnx[13]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'U'){																				// Ustecky (Usti nad Labem)
				p = knnx[14]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'Z'){																				// Zlinsky (Zlin)
				p = knnx[15]->findNearest(small_char_linear, 1, tmp);
			}		
			else{
				//cout << "P je prazdne.  " << char_plate[0] << endl;
			}
		}	
		
		// --------- DRUHY ZNAK PRO STARY TYP --------------------------
		if((j == 1) && (digitoralpha == 2)){
			if(char_plate[0] == 'A'){																					// Praha
				p = knnx[16]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'B'){																				// Benesov, Beroun, Blansko, Brno-mesto, Brno-okres, Bruntal, Breclav
				p = knnx[17]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'C'){	 																			// Ceska Lipa, Ceske Budejovice, Cesky Krumlov, Cheb, Chomutov, Chrudim
				p = knnx[18]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'D'){																				// Decin, Domazlice
				p = knnx[19]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'F'){	 																			// Frydek-Mistek
				p = knnx[20]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'H'){	 																			// Havlickuv Brod, Hodonin, Hradec Kralove
				p = knnx[21]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'J'){	 																			// Jablonec nad Nisou, Jesenik, Jihlava, Jindrichuv Hradec
				p = knnx[22]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'K'){	 																			// Karlovy Vary, Karvina, Kladno, Klatovy, Kolin, Kromeriz, Kutna Hora
				p = knnx[23]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'L'){	 																			// Liberec, Litomerice, Louny
				p = knnx[24]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'M'){	 																			// Melnik, Mlada Boleslav, Most
				p = knnx[25]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'N'){	 																			// Nachod, Nový Jicin, Nymburk
				p = knnx[26]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'O'){																				// Olomouc, Opava, Ostrava
				p = knnx[27]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'P'){																				// Pardubice, Pelhrimov, Pisek, Plzen-jih, Plzen-mesto, Plzen-sever, Praha-vychod, Praha-zapad, Prachatice, Prostejov, Prerov, Pribram
				p = knnx[28]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'R'){	  																			// Rakovník, Rokycany, Rychnov nad Kneznou
				p = knnx[29]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'S'){	 																			// Semily, Sokolov, Strakonice, Svitavy, Sumperk
				p = knnx[30]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'T'){	 																			// Tabor, Tachov, Teplice, Trutnov, Trebic
				p = knnx[31]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'U'){	 																			// Uherské Hradiste, Usti nad Labem, Usti nad Orlici
				p = knnx[32]->findNearest(small_char_linear, 1, tmp);	
			}
			else if(char_plate[0] == 'V'){																				// Vranov, Vsetin, Vyskov
				p = knnx[33]->findNearest(small_char_linear, 1, tmp);
			}
			else if(char_plate[0] == 'Z'){	 																			// Zlin, Znojmo, Zdar nad Sazavou
				p = knnx[34]->findNearest(small_char_linear, 1, tmp);	
			}
			else{	
				//cout << "P je prazdne.  " << char_plate[0] << endl;
			}
		}
		// --------- TRETI ZNAK PRO STARY TYP --------------------------
		else if((j == 2) && (digitoralpha == 2)){
			p = knnx[35]->findNearest(small_char_linear, 1, tmp);	
		}
		
		// --------- ZBYLE CISLICE -------------------------------------
        if(j >= 3){
			p = knnx[36]->findNearest(small_char_linear, 1, tmp);
		}
		plate.push_back(char(p));
		
    return plate;
}

string precognition(Mat plate_img, int j, int digitoralpha, string char_plate, Ptr<ml::KNearest> knnx[])
{
	plate_img = segment_image(plate_img);
	
	if(plate_img.empty()){					// prazdny obrazek z duvodu nedostatecne velke kontury znaku - pricina -> spatne rezani
		string empty = "empty";
		return empty;
	}
	
	string plate(process_image2(plate_img, j, digitoralpha, char_plate, knnx));
	return plate;

}
