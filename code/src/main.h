#ifndef MAIN_H
#define MAIN_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <iostream>
#include "background_change.h"
#include "background_different.h"
#include "detect_plate.h"
#include "symbol.h"

// ====================================================================

using namespace cv;  using namespace std;									

// ====================================================================

double const SCALE(5.0);												

// ====================================================================

int main(int, char**);													

#endif
