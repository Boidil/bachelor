#ifndef BACKGROUND_DIFFERENT_H
#define BACKGROUND_DIFFERENT_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <iostream>
#include "main.h"

// ====================================================================

using namespace std;
using namespace cv;
       
// ====================================================================         	
         	
typedef std::vector<cv::Point> point_vector;         	
typedef std::vector<point_vector> contour_vector;         	
         						
// ====================================================================         						
         						
Mat image_diff( Mat, Mat, Mat, Mat);

#endif
