#ifndef BACKGROUND_CHANGE_H
#define BACKGROUND_CHANGE_H

// ====================================================================
// ============================ KNIHOVNY ==============================
// ====================================================================

#include <opencv2/opencv.hpp>
#include <iostream>

// ====================================================================

using namespace std;
using namespace cv;
         						
// ====================================================================         						
         						
Mat background_substraction( Mat, int);

#endif
