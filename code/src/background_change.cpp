
#include "background_change.h"
#include "main.h"

Ptr<BackgroundSubtractor> pMOG2 = createBackgroundSubtractorMOG2(); 	//MOG2 Background subtractor

Mat background_substraction(Mat frame_gray, int frames){
    Mat new_background, fgMaskMOG2, frame_first;
	
    if(frames == 0){
      frame_first = imread("background/back1.png",CV_LOAD_IMAGE_GRAYSCALE);
      if(frame_first.empty()){
		  cout << "Chybi referencni vstupni pozadi." << endl;
		  exit(1);
	  }
      
      resize(frame_first, frame_first, Size(), (1.0 / SCALE), (1.0 / SCALE), INTER_AREA);
      pMOG2->apply(frame_first, fgMaskMOG2, 1);
      pMOG2->getBackgroundImage(new_background);
    }
    else{
	  pMOG2->apply(frame_gray, fgMaskMOG2, 0.001);
      pMOG2->getBackgroundImage(new_background);	
	}
    return new_background;
}
