// Uživatelská příručka
// Autor: Tomáš Hažmuka
// Téma: Re-identifikace automobilů pomocí registrační značky
 

--> Jsou k dispozici dve videa k testovani A.MTS a B.MTS 
--> Je vhodne spustit A.MTS k identifikaci a B.MTS k reidentifikaci

--> Program je treba prelozit prikazem: cmake .
--> Pote zadat: make

--> Program lze spustit buď s dvěma parametry - vstupním videem, v tomto módu video provádí pouze rozpoznání SPZ bez reidentifikace.
--> A vystupnim souborem na rozpoznane SPZ.
--> Příklad: ./Exe video.mp4 output.txt

--> Druhou možností je program spustit s třemi parametry. Tím třetím je vstupní soubor s rozpoznanými SPZ sloužící k reidentifikace.
--> Příklad: ./Exe video.mp4 output.txt plates.txt
--> V tomto případě budou do výstupního souboru uloženy pouze reidentifikované SPZ.
