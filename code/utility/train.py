import cv2
import numpy as np

# both
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "F", "H", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "Z"]
# numbers
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

# 2_new
#CHARS = ["A", "B", "C", "E", "H", "J", "K", "L", "M", "P", "S", "T", "U", "Z"]

# 3_new_a
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "H", "I"]
# 3_new_b
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "H", "I", "J", "K", "L"]
# 3_new_c
#CHARS = ["0", "1", "2", "3", "4", "5"]
# 3_new_e
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7"]
# 3_new_h
#CHARS = ["0", "1", "2", "3", "4", "5"]
# 3_new_j
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
# 3_new_k
#CHARS = ["0"]
# 3_new_l
#CHARS = ["0", "1", "2", "3"]
# 3_new_m
#CHARS = ["0", "1"]
# 3_new_p
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7"]
# 3_new_s
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "X", "Y"]
# 3_new_t
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C"]
# 3_new_u	
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
# 3_new_z	
#CHARS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

# 3_old	
#CHARS = ["A", "B", "C", "D", "E", "F", "H", "I", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "X", "Y", "Z"]

# 2_old_a
#CHARS = ["B", "C", "D", "E", "F", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "X", "Y", "Z"]
# 2_old_b
#CHARS = ["E", "I", "K", "M", "N", "O", "R", "S", "V", "Z"]
# 2_old_c
#CHARS = ["B", "E", "H", "K", "L", "R", "V"]
# 2_old_d
#CHARS = ["C", "O"]
# 2_old_f
#CHARS = ["I", "M"]
# 3_old_h
#CHARS = ["B", "K", "O", "R"]
# 2_old_j
#CHARS = ["C", "E", "H", "I", "N"]
# 2_old_k
#CHARS = ["A", "D", "H", "I", "L", "N", "O", "R", "T", "V"]
# 2_old_l
#CHARS = ["B", "I", "N", "T"]
# 2_old_m
#CHARS = ["B", "E", "O"]
# 2_old_n
#CHARS = ["A", "B", "J"]
# 2_old_o
#CHARS = ["C", "L", "M", "P", "S", "T", "V"]
# 3_old_p
#CHARS = ["A", "B", "C", "E", "H", "I", "J", "M", "N", "R", "S", "T", "U", "V", "Y", "Z"]
# 2_old_r
#CHARS = ["A", "K", "O"]
# 2_old_s
#CHARS = ["M", "O", "T", "U", "Y"]
# 2_old_t
#CHARS = ["A", "C", "P", "R", "U"]
# 2_old_u
#CHARS = ["H", "L", "O", "S"]
# 2_old_v
#CHARS = ["S", "V", "Y"]
# 2_old_z
CHARS = ["L", "N", "R"]


# ============================================================================

def load_char_images():
    characters = {}
    for char in CHARS:
        char_img = cv2.imread("chars/%s.png" % char, 0)
        characters[char] = char_img
    return characters

# ============================================================================

characters = load_char_images()

samples =  np.empty((0,1600))
for char in CHARS:
    char_img = characters[char]
    small_char = cv2.resize(char_img,(40,40))
    #print char_img
    #print small_char.shape
    sample = small_char.reshape(1,1600)
    samples = np.append(samples,sample,0)
    


responses = np.array([ord(c[0]) for c in CHARS],np.float32)
responses = responses.reshape((responses.size,1))

respsam = np.hstack((samples,responses))

np.savetxt("2_old_z.csv", respsam, delimiter=",")
#np.savetxt("0_both.csv", respsam, delimiter=",")
#np.savetxt("2_old_z.csv", respsam, delimiter=",")

# ============================================================================
